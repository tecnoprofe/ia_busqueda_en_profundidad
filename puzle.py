#PUZLE LINEAL CON BUSQUEDA EN AMPLITUD

from arbol import Nodo

def ubicar_los_indices_de_cero(matriz):
    for fila in range(3):
        for col in range(3):
            if matriz[fila][col]==0:
                return [fila,col]
            
def metodo_heuristico_UPDS(estado_inicial,solucion):
    solucionado=False
    nodos_visitados=[]
    nodos_frontera=[]
    nodoInicial=Nodo(estado_inicial)
    nodos_frontera.append(nodoInicial)
    while(not solucionado) and len(nodos_frontera)!=0:
        nodo=nodos_frontera.pop(0)
        #Extrae nodo y añade a los visitados
        nodos_visitados.append(nodo)
        if nodo.get_datos()==solucion:
            #solucion encontrada
            solucionado =True
            return nodo
        else:
            #expandir nodos hijo
            dato_nodo=nodo.get_datos()
            #Determinar  el cero
            indice=ubicar_los_indices_de_cero(dato_nodo)
            if indice[0]==0 and indice[1]==0:
                pass
            if indice[0]==0 and indice[1]==1:                
                
            
            if indice[0]==0 and indice[1]==2:
                pass
            if indice[0]==1 and indice[1]==0:
                pass
            
            
            

if __name__ == "__main__":
    estado_inicial=[
                        [1,0,3],
                        [4,5,6],
                        [7,2,8]
                        ]
    solucion=[
                [1,2,3],
                [4,5,6],
                [7,8,0]]
    nodo_solucion= metodo_heuristico_UPDS(estado_inicial,solucion)
    #nodo_solucion=Busqueda_en_Amplitud(estado_inicial,solucion)
    #mostrar resultado
    resultado=[]
    nodo=nodo_solucion
    while nodo.get_padre()!=None:
        resultado.append(nodo.get_datos())
        nodo=nodo.get_padre()

    resultado.append(estado_inicial)
    resultado.reverse()
    print(resultado) 