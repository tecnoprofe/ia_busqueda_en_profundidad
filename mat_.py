def ubicar_los_indices_de_cero(matriz):
    for fila in range(3):
        for col in range(3):
            if matriz[fila][col]==0:
                return [fila,col]

inicial=[
        [1,2,3],
        [4,0,6],
        [7,5,8]
    ]
indices=ubicar_los_indices_de_cero(inicial)
print(indices)


final=[
        [1,2,3],
        [4,5,6],
        [7,8,0]
    ]
